<?php

use yii\db\Migration;

class m130524_201443_alter_article_category extends Migration
{
    public function up()
    {
        $this->addColumn('article_category','status', $this->integer()->defaultValue(1));
    }

    public function down(){
        $this->dropColumn('article_category','status');
    }
}
