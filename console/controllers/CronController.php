<?php
namespace console\controllers;

use app\components\RssHelper;
use Yii;
use yii\console\Controller;
use keltstr\simplehtmldom\SimpleHTMLDom as SHD;
use common\models\News;
use common\models\Video;
use Madcoda\Youtube;

class CronController extends Controller
{
    protected $url = "http://www.michael-smirnov.ru/Get_RSS.php?pRSSurl=http%3A%2F%2Faif.ru%2Frss%2Fnews.php&pRSSdesc=1";
    protected $arrLinks = [];
    protected $arrNews = [];
    protected $limit = 16;
    protected $Iterator = 0;
    protected $p = '';

    public function actionInit()
    {
    }

    public function actionNews()
    {
        echo "Start getting news..." . PHP_EOL;

        $html = SHD::file_get_html($this->url);
        $fileNews = Yii::getAlias('@frontend/runtime/fileNews.txt');

        // Find all links
        foreach ($html->find('a.ahrefnews') as $link) {
            $this->Iterator++;
            if ($this->Iterator <= $this->limit)
                $arrLinks[] = $link->href;
        }
        // get data
        $Iterator = 0;
        foreach ($arrLinks as $link) {
            $Iterator++;
            $html2 = SHD::file_get_html($link);
            if (!empty($html2)) {
                $arrNews[$Iterator]['Title'] = $html2->find('h1.material_title', 0)->plaintext;
                $arrNews[$Iterator]['Image'] = $html2->find('div.article_img a.zoom_js', 0)->href;
                foreach ($html2->find('.material_content p') as $p) {
                    $this->p .= trim(preg_replace("/ {2,}/", " ", $p->plaintext));
                }
                $arrNews[$Iterator]['Content'] = trim($this->p);
            }
            $this->p = '';
        }

        file_put_contents($fileNews, serialize($arrNews));

        foreach ($arrNews as $row) {
            $modelBlog = new News();
            $modelBlog->title = $row['Title'];
            $modelBlog->content = $row['Content'];
            $modelBlog->author = 'admin';
            $modelBlog->image = $row['Image'];
            $modelBlog->view = 0;
            $dublicate = News::getDublicateByTitle($row['Title']);
            if (!$dublicate) {
                $modelBlog->save();
            }
        }
        echo "Finish...success" . PHP_EOL;
    }

    public function actionRemoveFalseVideo()
    {
        $model = Video::find()->all();
        $modelCount = Video::find()->count();
        $youtube = new Youtube(array('key' => 'AIzaSyBU4vsvP20CYdFuibdgTMOaZ10vt7JxV5c'));

        echo "Was find: ". $modelCount .  " videos".PHP_EOL;

        foreach ($model as $video){
            $videoOne = $youtube->getVideoInfo($video->youtube_id);
            if($videoOne == false){
                //vd(1,false);
                echo "Delete: ". $video->id. PHP_EOL;
                Video::findOne($video->id)->delete();
            }
            //vd(2,false);
        }
    }
}