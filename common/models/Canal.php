<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "canal".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property integer $status
 * @property integer $order
 * @property string $slug
 */
class Canal extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                //'slugAttribute' => 'slug',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'canal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'order'], 'integer'],
            [['name', 'description', 'code'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'code' => 'Код',
            'status' => 'Статус',
            'order' => 'Порядок',
            'slug' => 'Slug',
        ];
    }
}
