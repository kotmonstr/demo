<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $file_path
 * @property string $file_title
 * @property string $file_type
 * @property string $descr
 * @property string $created_at
 * @property string $updated_at
 * @property string $image
 * @property string $image_path
 */
class Files extends \yii\db\ActiveRecord
{
    public $file;
    public $temp_file;
    public $uploudPath = '/web/upload/files/';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_name', 'file_path', 'file_title', 'file_type', 'descr', 'image', 'image_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'Имя(реальное)',
            'file_path' => 'Путь',
            'file_title' => 'Имя(для пользователя)',
            'file_type' => 'Тип файла',
            'descr' => 'Описание',
            'created_at' => 'Время создания',
            'updated_at' => 'Время редактирования',
            'image' => 'Картинка',
            'image_path' => 'Image Path',
        ];
    }
    public function upload($newName)
    {
        if ($this->validate()) {
            $this->temp_file->saveAs(Yii::getAlias('@frontend').$this->file_path . $newName);
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            $fullPath = Yii::getAlias('@frontend')  .$this->uploudPath .$this->file_name;
            if(file_exists($fullPath)) {
                unlink($fullPath);
            }
            $fullPath2 = Yii::getAlias('@frontend')  .$this->uploudPath .$this->image;
            if(file_exists($fullPath2)) {
                unlink($fullPath2);
            }
            return true;
        } else {
            return false;
        }
    }

}
