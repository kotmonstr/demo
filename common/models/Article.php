<?php

namespace common\models;


use yii\db\Expression;
use yii\behaviors\SluggableBehavior;



/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $article_category
 * @property string $title
 * @property string $image
 * @property string $src
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $author
 * @property integer $updater_id
 * @property integer $view
 * @property integer $template
 * @property string $slug
 *
 * @property ArticleCategory $articleCategory
 * @property Template $template0
 */
class Article extends \yii\db\ActiveRecord
{
    public $file;
    public $filename;
    //public $slug;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_category', 'title', 'image', 'src', 'content', 'template'], 'required'],
            [['article_category', 'view', 'template','like','status'], 'integer'],
            [['content'], 'string'],
            [['title', 'image', 'src'], 'string', 'max' => 255],
            [['author'], 'string', 'max' => 100],
            [['article_category'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['article_category' => 'id']],
            [['template'], 'exist', 'skipOnError' => true, 'targetClass' => Template::className(), 'targetAttribute' => ['template' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_category' => 'Категория статьи',
            'title' => 'Наименование',
            'image' => 'Картинка',
            'src' => 'Src',
            'content' => 'Содержание',
            'created_at' => 'Время создания',
            'updated_at' => 'Updated At',
            'author' => 'Автор',
            'updater_id' => 'Updater ID',
            'view' => 'View',
            'template' => 'Template',
            'slug' => 'Slug',
            'status'=>'Статус'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'article_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template']);
    }


    /**
     * @inheritdoc
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    public static function getDublicateByTitle($title)
    {
        $model = self::find()->where(['title' => $title])->one();
        if ($model) {
            return true;
        } else {
            return false;
        }

    }
    public static function UpdateViewCount($id)
    {
        $model = self::findOne($id);
        $model->view = $model->view+1;
        $model->save();
       //vd($model);
    }

    public static function getViewCount($id)
    {
        $model = self::findOne($id);
        return $model->view;
    }
}
