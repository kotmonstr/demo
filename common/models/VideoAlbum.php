<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "video_album".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 */
class VideoAlbum extends \yii\db\ActiveRecord
{
    public function behaviors()
    {


        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => new Expression('NOW()'),
            ],
        ];


    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'slug' => 'Slug',
            'created_at' => 'Созданно',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getIdBySlug($slug){
        $model = self::find()->where(['slug'=> $slug])->one();
        return $model ? $model->id : false;
    }
}
