window.onload = initMain;

function initMain() {

    var canvas = new fabric.Canvas('c', {});
    var Circle = new fabric.Circle({top: 140, left: 230, radius: 75, fill: 'green'});
    var text = 'Kotmonstr ru';
    var text2 = '.';
    var Text = new fabric.Text(text, {
        centeredRotation: false,
        rotatingPointOffset: 40,
        fontFamily: 'Impact',
        fontStyle: 'italic',
        // fontWeight: 'bold',
        fontSize: 40,
        fill: '#99cc66',
        shadow: 'rgba(0,0,0,0.5) 3px 3px 10px',
        hoverCursor: 'pointer',
        charSpacing: 10,
        selectable: false,
        stroke: 'white',
        strokeWidth: .3
    });

    var DOT = new fabric.Rect({
        left: 179,
        top: 31,
        fill: '#99cc66',
        width: 8,
        height: 8,
        originX: 'center',
        originY: 'center',
        shadow: 'rgba(0,0,0,0.5) 3px 3px 10px',
        hoverCursor: 'pointer',
        selectable: false,
        stroke: 'white',
        strokeWidth: .3
    });

    var BackGroundGray = new fabric.Rect({
        left: 0,
        top: 30,
        fill: '#f7f3ec',
        width: 300,
        height: 15,
        hoverCursor: 'pointer',
        selectable: false
        //originX: 'center',
        //originY: 'center'
    });

    var BackGroundRed = new fabric.Rect({
        left: 0,
        top: 45,
        fill: 'blue',
        width: 300,
        height: 15,
        hoverCursor: 'pointer',
        selectable: false
        //originX: 'center',
        //originY: 'center'
    });

    var BackGroundBlue = new fabric.Rect({
        left: 0,
        top: 60,
        fill: 'red',
        width: 300,
        height: 15,
        hoverCursor: 'pointer',
        selectable: false
        //originX: 'center',
        //originY: 'center'
    });

    var group = new fabric.Group([BackGroundGray, BackGroundRed, BackGroundBlue], {
        left: 300,
        top: 0
    });


    canvas.add(group, Text, DOT);

    canvas.on('mouse:over', function (options) {
        if (options.target) {
            canvas.item(2).animate('angle', '+=99360', {
                duration: 30000,
                onChange: canvas.renderAll.bind(canvas)
            });

            canvas.item(0).animate('left', 0, {
                duration: 1000
            });
        }
    });

    canvas.on('mouse:out', function (options) {
        if (options.target) {
            canvas.item(0).animate('left', 300, {
                duration: 500
            });
        }
    });


}