<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class MainMeghnaAsset extends AssetBundle
{
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        'MEGHNA/css/font-awesome.min.css',
        'MEGHNA/css/bootstrap.min.css',
        'MEGHNA/css/animate.css',
        'MEGHNA/css/owl.carousel.css',
        'MEGHNA/css/component.css',
        'MEGHNA/css/slit-slider.css',
        'MEGHNA/css/main.css',
        'MEGHNA/css/media-queries.css',
        'http://fonts.googleapis.com/css?family=Oswald:400,300',
        'http://fonts.googleapis.com/css?family=Ubuntu:400,300',

// slider
        //'REVOLUTION/css/settings.css',
       // 'REVOLUTION/css/layers.css',
       // 'REVOLUTION/css/navigation.css',
    ];

    public $js = [
        'MEGHNA/js/jquery-1.11.0.min.js',
        'MEGHNA/js/bootstrap.min.js',
        'MEGHNA/js/jquery.slitslider.js',
        'MEGHNA/js/jquery.ba-cond.min.js',
        'MEGHNA/js/jquery.parallax-1.1.3.js',
        'MEGHNA/js/owl.carousel.min.js',
        'MEGHNA/js/jquery.mixitup.min.js',
        'MEGHNA/js/jquery.nicescroll.min.js',
        'MEGHNA/js/jquery.appear.js',
        'MEGHNA/js/easyPieChart.js',
        'MEGHNA/js/jquery.easing-1.3.pack.js',
        'MEGHNA/js/tweetie.min.js',
        'MEGHNA/js/http://maps.google.com/maps/api/js?sensor=false',
        'MEGHNA/js/jquery.nav.js',
        'MEGHNA/js/jquery.sticky.js',
        'MEGHNA/js/jquery.countTo.js',
        'MEGHNA/js/wow.min.js',
        'MEGHNA/js/jquery.fitvids.js',
        'MEGHNA/js/grid.js',
        'MEGHNA/js/custom.js',
        'MEGHNA/js/modernizr-2.6.2.min.js',
//slider
        //'REVOLUTION/js/jquery.themepunch.tools.min.js',
        //'REVOLUTION/js/jquery.themepunch.revolution.min.js?rev=5.0',

    ];
    public $depends = [

        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',

    ];
}

