<?php
namespace app\components;

use Yii;
use keltstr\simplehtmldom\SimpleHTMLDom as SHD;

class RssHelper
{
    protected $url = "http://www.michael-smirnov.ru/Get_RSS.php?pRSSurl=http%3A%2F%2Faif.ru%2Frss%2Fnews.php&pRSSdesc=1";
    protected $arrLinks=[];
    protected $arrNews=[];
    protected $limit=16;
    protected $Iterator=0;
    protected $p='';

    public function getUrls(){
        $html = SHD::file_get_html($this->url);
        $fileNews = Yii::getAlias('@frontend/runtime/fileNews.txt');

        // Find all links
        foreach($html->find('a.ahrefnews') as $link) {
            $this->Iterator++;
            if($this->Iterator <= $this->limit  )
            $arrLinks[] = $link->href;
        }
        // get data
        $Iterator=0;
        foreach ($arrLinks as $link){
            $Iterator++;
            $html2 = SHD::file_get_html($link);
            if(!empty($html2)) {
                $arrNews[$Iterator]['Title'] = $html2->find('h1.material_title',0)->plaintext;
                $arrNews[$Iterator]['Image'] = $html2->find('div.article_img a.zoom_js',0)->href;
                foreach ($html2->find('.material_content p') as $p) {
                    $this->p .= trim(preg_replace("/ {2,}/"," ",$p->plaintext));
                }
                $arrNews[$Iterator]['Content'] = trim($this->p);
            }
            $this->p='';
        }
        //vd($arrNews);
        file_put_contents($fileNews,serialize($arrNews));
        //vd('ok');
        return $arrNews;
    }
}