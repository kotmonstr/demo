<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
?>
<div class="container marg75">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="title"><span>Новые статьи</span></h3>
            <div class="pagination">
                <a href="" id="portfolio_left" class="prev"></a>
                <a href="" id="portfolio_right" class="next"></a>
            </div>
        </div>
        <div class="col-lg-12">
            <div id="portfolio" class="marg25">
                <div class="showbiz" data-left="#portfolio_left" data-right="#portfolio_right">
                    <div class="overflowholder">
                        <ul>

                            <? if($model): ?>
                                <? foreach ($model as $article): ?>
                                    <li class="portfolio-block">
                                    <div class="mediaholder">
                                        <div class="mediaholder_innerwrap">
                                            <a href="<?= Url::to(['/site/article-detail','slug'=> $article->slug]); ?>"><?= Html::img($article->src. '/'.$article->image,['alt'=>'']) ?></a>
<!--                                            <div class="hovercover" data-maxopacity="0.85">-->
<!--                                                <a href="--><?//= Url::to(['/site/article-detail','slug'=> $article->slug]); ?><!--"><div class="linkicon notalone"><i class="fa fa-link"></i></div></a>-->
<!--                                                <a class="fancybox" rel="group2" href="/GrandWay/assets/images/11.jpg"><div class="lupeicon notalone"><i class="fa fa-search"></i></div></a>-->
<!--                                            </div>-->
                                        </div>
                                    </div>
                                    <div class="detailholder">
                                        <div class="portfolio-name"><?= StringHelper::truncate($article->title,30) ?></div>
<!--                                        <div class="portfolio-text">--><?//= strip_tags(Html::decode(StringHelper::truncate($article->content,30))) ?><!--</div>-->
                                    </div>
                                </li>
                                <? endforeach; ?>
                            <? endif; ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>