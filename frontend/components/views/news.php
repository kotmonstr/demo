<?php
use yii\helpers\Html;
use frontend\assets\GrandwayAsset;
use yii\helpers\Url;

$this->registerJsFile('/js/get-news.js',['depends'=> GrandwayAsset::className()]);

?>

<div class="twitter_fot marg25">
    <div class="container ">
        <div class="row">
            <div class="col-lg-11 col-md-10">

                <? if($model): ?>
                        <div class="tweet">Последняя новости: <?= Html::a($model->title, Url::to(['/site/news-detail','slug' => $model->slug]),['id'=>'news-one']) ?></div>
                <? endif; ?>

            </div>
            <div class="col-lg-1  col-md-2 hidden-pag">
                <div class="paginat">
                    <a id="prev" onclick="getNewsBefor('<?= isset($model->id) ? $model->id : false; ?>')"><i class="fa fa-arrow-left"></i></a>
                    <a id="next" onclick="getNewsAfter('<?= isset($model->id) ? $model->id : false; ?>')"><i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
