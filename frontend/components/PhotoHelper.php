<?php
namespace app\components;
use common\models\Goods;
use common\models\Image;
use common\models\ImageSlider;
use common\models\Photo;
use yii\helpers\BaseArrayHelper;
use common\models\Article;
use common\models\Banner;
use common\models\Book;



class PhotoHelper extends BaseArrayHelper
{
    public static function isPhotoDeletable($name)
    {
        $result = [];
        $findOne = Article::find()->where(['image'=>$name])->one();
        if($findOne){ $result[]='Article';}
        $findOne = Banner::find()->where(['name'=>$name])->one();
        if($findOne){ $result[]='Banner';}
        $findOne = Book::find()->where(['image'=>$name])->one();
        if($findOne){ $result[]='Book';}
        $findOne = Goods::find()->where(['image'=>$name])->one();
        if($findOne){ $result[]='Goods';}
        $findOne = ImageSlider::find()->where(['name'=>$name])->one();
        if($findOne){ $result[]='ImageSlider';}
        $findOne = Photo::find()->where(['name'=>$name])->one();
        if($findOne){ $result[]='Photo';}


        if(!empty($result)){
            return $result[0];
        }else{
            return false;
        }


    }
}