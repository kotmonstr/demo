<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <h1><?= Html::encode($this->title) ?></h1>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            //'auth_key',
           // 'password_hash',
            //'password_reset_token',
             'email:email',
             //'status',
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter'=> $arrStatus,
                'headerOptions' => ['width' => '180'],
                'contentOptions' =>['style'=>'width:180px'],
                'value' => function ($dataProvider) {
                    return User::getStatus($dataProvider->status);
                }
            ],
             //'created_at',
            [

                'attribute' => 'created_at',
                //'format' => 'date',
                'value' => function($data){
                    return Yii::$app->formatter->asDate($data->created_at, 'long');
                }
            ],
            // 'updated_at',
             'password',
            [
                'attribute' => 'role',
                'format' => 'html',
                'filter'=> $arrRole,
                'value' => function ($dataProvider) {
                    return User::getRole($dataProvider->role);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
            </div>
        </div>
    </div>

</section>