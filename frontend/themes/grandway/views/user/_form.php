<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'status')->dropDownList([10 => 'Активен', 0 => 'Неактивен'],['selected'=> \common\models\User::getStatus($model->status)]) ?>

    <?= $form->field($model, 'role')->textInput() ?>

    <?= $form->field($model, 'role')->dropDownList([1 => 'Пользователь', 5 => 'Модератор',10 => 'Администратор'],['selected'=> \common\models\User::getRole($model->role)]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>