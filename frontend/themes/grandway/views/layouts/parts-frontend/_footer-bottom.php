<div class="footer_bottom">
    <div class="container ">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-ms-6 pull-left col-ms-12">
                <div class="copyright">Copyright <?= date('Y') ?> Alternative Way. Design by <a href="http://kotmonstr.com" target="_blank">Kotmonstr</a></div>
            </div>
            <div class="col-lg-6 col-md-6 col-ms-6 pull-right col-ms-12">
                <div class="foot_menu">
                    <ul>
                        <li><a href="/">Главная</a></li>
                        <li><a href="/article/index">Статьи</a></li>
                        <li><a href="/site/gallery">Картинки</a></li>
                        <li><a href="/site/video">Видео</a></li>
                        <li><a href="/site/news" target="_blank">Новости</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>