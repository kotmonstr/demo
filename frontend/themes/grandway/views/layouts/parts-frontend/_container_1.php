<div class="container marg75">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-xs-4 col-ms-4">
            <div class="iconbox"><span class="one"></span><br>
                <h3>Статьи</h3>
                <p>Сборник актуальных статей , дополненых артефактами , аргуметированных  и фото-видео доказательствами.</p>
                <p class="read"><a href="/article/index">Читать →</a></p>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-4 col-ms-4">
            <div class="iconbox"><span class="iconbox-mac"></span><br>
                <h3>Фото докуметы</h3>
                <p>Сборник невероятных фотографий - свидетельств сногшибательных фактов и артефактов истории.</p>
                <p class="read"><a href="/site/gallery">Смотреть →</a></p>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-4 col-ms-4">
            <div class="iconbox"><span class="iconbox-search"></span><br>
                <h3>Видео</h3>
                <p>Сборник документальных видео фильмов , роликов , подтверждающих и агруметирующих секретные материалы.</p>
                <p class="read"><a href="/site/video">Смотреть →</a></p>
            </div>
        </div>
    </div>
</div>