<div class="container marg75">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="title"><span>Some of Our Clients</span></h3>
            <div class="pagination">
                <a href="" id="clients_left" class="prev"></a>
                <a href="" id="clients_right" class="next"></a>
            </div>
        </div>
    </div>
    <div class="row marg25">
        <div class="col-lg-12">
            <div id="clients" >
                <div class="showbiz" data-left="#clients_left" data-right="#clients_right">
                    <div class="overflowholder">
                        <ul>
                            <li>
                                <div class="mediaholder_innerwrap">
                                    <a href="#"><img alt="" src="/GrandWay/assets/images/5_5.png"><div class="reveal_opener_clients show_on_hover"><img alt="" src="assets/images/5.png"></div></a>
                                </div>
                            </li>
                            <li>
                                <div class="mediaholder_innerwrap">
                                    <a href="#"><img alt="" src="/GrandWay/assets/images/2_2.png"><div class="reveal_opener_clients show_on_hover"><img alt="" src="assets/images/2.png"></div></a>
                                </div>
                            </li>
                            <li>
                                <div class="mediaholder_innerwrap">
                                    <a href="#"><img alt="" src="/GrandWay/assets/images/3_3.png"><div class="reveal_opener_clients show_on_hover"><img alt="" src="assets/images/3.png"></div></a>
                                </div>
                            </li>
                            <li>
                                <div class="mediaholder_innerwrap">
                                    <a href="#"><img alt="" src="/GrandWay/assets/images/6_6.png"><div class="reveal_opener_clients show_on_hover"><img alt="" src="assets/images/6.png"></div></a>
                                </div>
                            </li>
                            <li>
                                <div class="mediaholder_innerwrap">
                                    <a href="#"><img alt="" src="/GrandWay/assets/images/4_4.png"><div class="reveal_opener_clients show_on_hover"><img alt="" src="assets/images/4.png"></div></a>
                                </div>
                            </li>
                            <li>
                                <div class="mediaholder_innerwrap">
                                    <a href="#"><img alt="" src="/GrandWay/assets/images/1_1.png"><div class="reveal_opener_clients show_on_hover"><img alt="" src="assets/images/1.png"></div></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>