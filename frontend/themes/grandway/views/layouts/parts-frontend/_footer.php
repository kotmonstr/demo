<?php
use app\components\RecentGoodsWidget;
use app\components\ReqvizitWidget;
use app\components\NewsWidget;

?>




<?= NewsWidget::widget() ?>


<div class="footer">

        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="footer-block"><img src="/GrandWay/assets/images/logo_foot.png" alt="" class="foot_marg">
                    </div>
                    <div class="foot-text">Сайт с позновательным контентом.</div>
                    <div class="foot-text"><!-- Yandex.Metrika informer --> <a href="https://metrika.yandex.ru/stat/?id=35920145&amp;from=informer" target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/35920145/3_0_000000FF_000000FF_1_pageviews" style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="35920145" data-lang="ru" /></a> <!-- /Yandex.Metrika informer --> <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35920145 = new Ya.Metrika({ id:35920145, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/35920145" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter --></div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="footer-block">Контакты</div>
                    <?= ReqvizitWidget::widget() ?>
                </div>
                <div class="col-lg-3 col-md-3">
                    <?= RecentGoodsWidget::widget(['template' => 'footer', 'limit' => 9]) ?>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="footer-block">Flickr Widget</div>
                    <div class="widget_flickr">
                        <div class="flickr_container">
                            <script type="text/javascript"
                                    src="http://www.flickr.com/badge_code_v2.gne?count=6&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=146823312@N06"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>