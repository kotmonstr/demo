<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Madcoda\Youtube;

/* @var $this yii\web\View */
/* @var $model common\models\Canal */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Canals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="canal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

<?php
    $youtube = new Youtube(array('key' => 'AIzaSyBU4vsvP20CYdFuibdgTMOaZ10vt7JxV5c'));
    $video = $youtube->getVideoInfo($model->code);
    $title = $video->snippet->title;
    $descr = $video->snippet->description;
    $imageSrc = $video->snippet->thumbnails->medium->url;


   ?>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute'=>'Фото',
                'format'=>'html',
                'value'=> '<img src="'.$imageSrc.'" width="100" height="70">'

            ],
            'description',
            'code',
            'status',
            [
                'attribute'=>'status',
                'value'=> $model->status == 1  ? 'Aктивен': 'Не активен'

            ],
            'order',
            'slug',
        ],
    ]) ?>

</div>
