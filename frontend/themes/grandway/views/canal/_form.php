<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Canal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="canal-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'onkeyup' => 'sendYoutubeCodeToCanal()', 'onchange' => 'sendYoutubeCodeToCanal()']) ?>

    <div class=" info">
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(['1'=>'Aктивный',0=>'Не активный']) ?>

    <?//= $form->field($model, 'order')->textInput() ?>

    <?//= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
